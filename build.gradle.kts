import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmCompile

buildscript {
    repositories {
        mavenCentral()
    }
}

plugins {
    java
    idea
    id("io.quarkus") version "2.4.2.Final"
    kotlin("jvm") version "1.6.0"
    kotlin("plugin.allopen") version "1.6.0"
    id("com.github.ben-manes.versions") version "0.39.0"

    id("com.diffplug.spotless") version "6.0.0"
}

group = "uk.co.borismorris.fireboard"
version = "0.0.1-SNAPSHOT"

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        jvmTarget = "17"
        freeCompilerArgs = listOfNotNull(
            "-Xjsr305=strict",
            "-Xjvm-default=enable",
            "-Werror"
        )
    }
}

allOpen {
    annotation("javax.enterprise.context.ApplicationScoped")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation(platform("io.quarkus:quarkus-bom:2.4.2.Final"))
    implementation("io.quarkus:quarkus-core")
    implementation("io.quarkus:quarkus-arc")
    implementation("io.quarkus:quarkus-kotlin")
    implementation("io.quarkus:quarkus-jackson")
    implementation("io.quarkus:quarkus-scheduler")
    implementation("io.quarkus:quarkus-smallrye-fault-tolerance")
    implementation("io.quarkus:quarkus-container-image-jib")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("com.influxdb:influxdb-client-java:3.4.0") {
        exclude(group = "io.reactivex.rxjava2", module = "rxjava")
    }
    implementation("com.squareup.retrofit2:converter-jackson:2.9.0")

    implementation("io.github.microutils:kotlin-logging:2.0.11")
    runtimeOnly("org.jboss.slf4j:slf4j-jboss-logmanager")

    testImplementation("io.quarkus:quarkus-junit5")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("org.assertj:assertj-core")
    testImplementation("org.awaitility:awaitility:4.0.3")
    testImplementation(platform("org.eclipse.jetty:jetty-bom:9.4.43.v20210629"))
    testImplementation("com.github.tomakehurst:wiremock-jre8:2.31.0") {
        exclude(group = "org.eclipse.jetty", module = "jetty-alpn-conscrypt-client")
        exclude(group = "org.eclipse.jetty", module = "jetty-alpn-conscrypt-server")
        exclude(group = "org.conscrypt")
        exclude(group = "commons-fileupload")
    }
    testImplementation("org.eclipse.jetty:jetty-alpn-java-server")
    testImplementation("org.mockito:mockito-junit-jupiter")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation(kotlin("test-junit5"))
}

spotless {
    kotlin {
        ktlint("0.43.0")
    }
    kotlinGradle {
        ktlint("0.43.0")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()

    systemProperty("java.util.logging.manager", "org.jboss.logmanager.LogManager")

    testLogging {
        exceptionFormat = FULL
        showStandardStreams = true
        events("skipped", "failed")
    }
}
