FROM registry.access.redhat.com/ubi8/ubi-minimal:latest

ARG APPLICATION_VERSION

RUN mkdir -p /opt/fireboard-influx

WORKDIR /opt/fireboard-influx

COPY /build/fireboard-influx-${APPLICATION_VERSION}-runner fireboard-influx

ENTRYPOINT ["./fireboard-influx"]