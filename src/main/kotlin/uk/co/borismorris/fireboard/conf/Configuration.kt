package uk.co.borismorris.fireboard.conf

import io.smallrye.config.ConfigMapping
import java.net.URL
import java.time.Duration
import java.util.Optional

@ConfigMapping(prefix = "fireboard")
interface FireboardApiConfig {
    fun url(): URL
    fun token(): String
}

@ConfigMapping(prefix = "influx")
interface InfluxConfig {
    fun url(): URL
    fun bucket(): String
    fun organization(): String
    fun authToken(): Optional<String>
}

@ConfigMapping(prefix = "metric")
interface MetricCollectionConfig {
    fun period(): Duration
}
