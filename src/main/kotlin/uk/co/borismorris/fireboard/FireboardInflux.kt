package uk.co.borismorris.fireboard

import io.quarkus.runtime.Quarkus
import io.quarkus.runtime.QuarkusApplication
import io.quarkus.runtime.annotations.QuarkusMain
import io.quarkus.scheduler.Scheduled
import io.quarkus.scheduler.Scheduled.ConcurrentExecution.SKIP
import mu.KLogging
import mu.KotlinLogging.logger
import uk.co.borismorris.fireboard.cloud.FireboardApi
import uk.co.borismorris.fireboard.cloud.FireboardDevice
import javax.enterprise.context.ApplicationScoped

@QuarkusMain
class FireboardInflux : QuarkusApplication {
    override fun run(vararg args: String?): Int {
        Quarkus.waitForExit()
        return 0
    }
}

@ApplicationScoped
class FireboardTemperaturePoller(private val fireboardApi: FireboardApi) {

    companion object : KLogging()

    val devices by lazy { fireboardApi.devices() }

    @Scheduled(identity = "fireboardDevicePoller", every = "{metric.period}", concurrentExecution = SKIP)
    fun onPoll() {
        devices.map { it to it.temperatureReadings() }.forEach { logger.info("Readings {} for Device {}", it.second, it.first.uuid) }
    }

    fun FireboardDevice.temperatureReadings() = fireboardApi.temperatureReadings(uuid)
}
