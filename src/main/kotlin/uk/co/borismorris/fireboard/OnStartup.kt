package uk.co.borismorris.fireboard

import io.quarkus.runtime.StartupEvent
import mu.KLogging
import uk.co.borismorris.fireboard.conf.FireboardApiConfig
import uk.co.borismorris.fireboard.conf.InfluxConfig
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.Observes

@ApplicationScoped
class OnStartup(private val influxConfig: InfluxConfig, private val fireboardApiConfig: FireboardApiConfig) {
    companion object : KLogging()

    fun onStart(@Observes @Suppress("UNUSED_PARAMETER") ev: StartupEvent?) {
        logger.info("Fireboard API config: {}", fireboardApiConfig)
        logger.info("Influx config: {}", influxConfig)
    }
}
