package uk.co.borismorris.fireboard.influx

import com.influxdb.client.write.Point

interface PointConverter<T : Any> {
    fun convert(input: T): Point
}
