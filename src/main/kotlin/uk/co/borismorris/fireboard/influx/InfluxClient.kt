package uk.co.borismorris.fireboard.influx

import com.influxdb.client.InfluxDBClientFactory
import com.influxdb.client.InfluxDBClientOptions
import com.influxdb.client.write.Point
import uk.co.borismorris.fireboard.conf.InfluxConfig
import javax.enterprise.context.ApplicationScoped

interface InfluxClient {
    fun writePoint(point: Point)
}

@ApplicationScoped
class JavaCientInfluxClient(influxConfig: InfluxConfig) : InfluxClient {
    val influxDBClient = InfluxDBClientOptions.builder()
        .url(influxConfig.url().toString())
        .org(influxConfig.organization())
        .bucket(influxConfig.bucket())
        .also { influxConfig.authToken().ifPresent { token -> it.authenticateToken(token.toCharArray()) } }
        .build()
        .let { InfluxDBClientFactory.create(it) }

    val influxWrite = influxDBClient.writeApiBlocking

    override fun writePoint(point: Point) {
        influxWrite.writePoint(point)
    }
}
