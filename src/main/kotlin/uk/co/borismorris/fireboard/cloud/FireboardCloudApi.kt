package uk.co.borismorris.fireboard.cloud

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import io.quarkus.runtime.annotations.RegisterForReflection
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path
import uk.co.borismorris.fireboard.conf.FireboardApiConfig
import java.math.BigDecimal
import java.net.InetAddress
import java.time.ZonedDateTime
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Produces

class FireboardCloudApiFactory(private val objectMapper: ObjectMapper, private val fireboardConfig: FireboardApiConfig) {
    @Produces
    @ApplicationScoped
    fun fireboardCloudApi() = Retrofit.Builder()
        .baseUrl(fireboardConfig.url().toString())
        .client(OkHttpClient.Builder().addInterceptor(AuthenticationInterceptor()).build())
        .addConverterFactory(JacksonConverterFactory.create(objectMapper))
        .build()
        .create<FireboardCloudApi>()

    private inner class AuthenticationInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain) = chain.request()
            .newBuilder()
            .header("Authorization", "Token ${fireboardConfig.token()}")
            .header("Accept", "application/json")
            .build()
            .let { chain.proceed(it) }
    }
}

@RegisterForReflection
interface FireboardCloudApi {

    @GET("devices.json")
    fun devices(): Call<List<FireboardDevice>>

    @GET("devices/{device}.json")
    fun device(@Path("device") deviceId: String): Call<FireboardDevice>

    @GET("sessions.json")
    fun sessions(): Call<List<FireboardSession>>

    @GET("sessions/{session}.json")
    fun session(@Path("id") sessionId: String): Call<FireboardSession>

    @GET("devices/{device}/temps.json")
    fun temperatureReadings(@Path("device") deviceId: String): Call<List<FireboardTemperatureReading>>
}

@RegisterForReflection
data class FireboardDevice(
    @JsonProperty("uuid") val uuid: String,
    @JsonProperty("id") val id: Int,
    @JsonProperty("title") val title: String,
    @JsonProperty("hardware_id") val hardwareId: String,
    @JsonProperty("created") val created: ZonedDateTime,
    @JsonProperty("latest_temps") val latestTemperatureReadings: List<FireboardTemperatureReading>,
    @JsonProperty("device_log") val deviceLog: DeviceLog
) {
    @RegisterForReflection
    data class DeviceLog(
        @JsonProperty("publicIP")
        @JsonDeserialize(using = InetAddressDeserializer::class)
        val publicIp: InetAddress
    )
}

@RegisterForReflection
data class FireboardSession(@JsonProperty("id") val id: Int)

@RegisterForReflection
data class FireboardTemperatureReading(
    @JsonProperty("channel") val channel: Int,
    @JsonProperty("degreetype") val degreeType: DegreeType,
    @JsonProperty("temp") val temperature: BigDecimal,
    @JsonProperty("created") val created: ZonedDateTime
)

@RegisterForReflection
enum class DegreeType(val id: Int) {
    CELSIUS(1),
    FAHRENHEIT(2);

    companion object {
        @JvmStatic
        @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
        fun forId(id: Int) = values().first { it.id == id }
    }
}

class InetAddressDeserializer : StdDeserializer<InetAddress>(InetAddress::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext) = InetAddress.getByName(_parseString(p, ctxt))
}
