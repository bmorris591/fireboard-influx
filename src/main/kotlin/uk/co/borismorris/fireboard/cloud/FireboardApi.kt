package uk.co.borismorris.fireboard.cloud

import retrofit2.Call
import javax.enterprise.context.ApplicationScoped

interface FireboardApi {
    fun devices(): List<FireboardDevice>

    fun device(deviceId: String): FireboardDevice

    fun sessions(): List<FireboardSession>

    fun session(sessionId: String): FireboardSession

    fun temperatureReadings(deviceId: String): List<FireboardTemperatureReading>
}

@ApplicationScoped
class FireboardApiCloudApi(private val cloudApi: FireboardCloudApi) : FireboardApi {
    override fun devices(): List<FireboardDevice> = cloudApi.devices().body()

    override fun device(deviceId: String) = cloudApi.device(deviceId).body()

    override fun sessions() = cloudApi.sessions().body()

    override fun session(sessionId: String) = cloudApi.session(sessionId).body()

    override fun temperatureReadings(deviceId: String) = cloudApi.temperatureReadings(deviceId).body()

    private inline fun <reified T> Call<T>.body(): T = execute().let {
        if (it.isSuccessful) it.body()!! else throw FireboardApiException("Failed to query API", it.code(), it.errorBody()?.string())
    }
}

class FireboardApiException(message: String, val code: Int, val body: Any?) : Exception(message)
