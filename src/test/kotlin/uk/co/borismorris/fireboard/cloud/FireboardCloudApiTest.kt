package uk.co.borismorris.fireboard.cloud

import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.QuarkusTestProfile
import io.quarkus.test.junit.QuarkusTestProfile.TestResourceEntry
import io.quarkus.test.junit.TestProfile
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import uk.co.borismorris.fireboard.WiremockResource
import uk.co.borismorris.fireboard.WiremockResource.Companion.RESOURCE_ROOT
import javax.inject.Inject

@QuarkusTest
@TestProfile(ApiTestProfile::class)
internal class FireboardCloudApiTest {

    @Inject
    lateinit var cloudApi: FireboardCloudApi

    @Test
    fun `When I request devices then devices are returned`() {
        val devices = cloudApi.devices().execute().body()

        assertThat(devices).hasSize(1)
    }

    @Test
    fun `Given there are recent temp readings When I request recent temp readings Then devices are returned`() {
        val readings = cloudApi.temperatureReadings("fcacd7d5-9165-44cd-998f-3c52c8afbed5").execute().body()

        assertThat(readings).hasSize(2)
    }
}

class ApiTestProfile : QuarkusTestProfile {
    override fun getConfigOverrides() = mutableMapOf("fireboard.token" to "a-fireboard-token", "influx.url" to "http://influx.invalid", "quarkus.scheduler.enabled" to "false")

    override fun testResources() = mutableListOf(TestResourceEntry(WiremockResource::class.java, mutableMapOf(RESOURCE_ROOT to "/uk/co/borismorris/fireboard/cloud/wiremock")))
}
