package uk.co.borismorris.fireboard

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.moreThanOrExactly
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import io.quarkus.test.junit.QuarkusTest
import io.quarkus.test.junit.QuarkusTestProfile
import io.quarkus.test.junit.QuarkusTestProfile.TestResourceEntry
import io.quarkus.test.junit.TestProfile
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import uk.co.borismorris.fireboard.WiremockResource.Companion.RESOURCE_ROOT
import java.util.concurrent.TimeUnit.MINUTES
import java.util.concurrent.TimeUnit.SECONDS

@QuarkusTest
@TestProfile(IntegrationTestProfile::class)
class FireboardInfluxIntegrationTest {

    @InjectServer
    lateinit var server: WireMockServer

    @BeforeEach
    fun init() {
        WireMock.configureFor(server.port())
    }

    @Test
    fun `Application starts successfully`() {
        await().pollInSameThread().atMost(1, MINUTES).pollInterval(1, SECONDS).untilAsserted {
            verify(moreThanOrExactly(1), getRequestedFor(urlPathEqualTo("/fireboard/devices/fcacd7d5-9165-44cd-998f-3c52c8afbed5/temps.json")))
        }
    }
}

class IntegrationTestProfile : QuarkusTestProfile {
    override fun getConfigOverrides() = mutableMapOf("fireboard.token" to "aabbccddeeff")

    override fun testResources() = mutableListOf(TestResourceEntry(WiremockResource::class.java, mutableMapOf(RESOURCE_ROOT to "/uk/co/borismorris/fireboard/wiremock")))
}
