package uk.co.borismorris.fireboard

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.configureFor
import com.github.tomakehurst.wiremock.common.Slf4jNotifier
import com.github.tomakehurst.wiremock.core.Options
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.jvm.javaField

class WiremockResource : QuarkusTestResourceLifecycleManager {
    companion object {
        const val RESOURCE_ROOT = "wiremock.resource-root"
        const val PORT = "wiremock.port"
        const val DEBUG = "wiremock.debug"
    }

    lateinit var options: WireMockConfiguration
    lateinit var server: WireMockServer

    override fun init(initArgs: MutableMap<String, String>) {
        val options = WireMockConfiguration.wireMockConfig()
            .jettyAcceptors(1)
            .jettyAcceptQueueSize(1024)
            .asynchronousResponseEnabled(false)

        initArgs[RESOURCE_ROOT]?.let { options.usingFilesUnderClasspath(it) }
        (initArgs[PORT]?.toIntOrNull() ?: Options.DYNAMIC_PORT).also { options.port(it) }
        (initArgs[DEBUG]?.toBoolean() ?: false).also { options.notifier(Slf4jNotifier(it)) }

        this.options = options
    }

    override fun start(): MutableMap<String, String> {
        server = WireMockServer(options)
        server.start()

        configureFor(server.port())

        return mutableMapOf(
            "influx.url" to "${server.baseUrl()}/influx/",
            "fireboard.url" to "${server.baseUrl()}/fireboard/",
        )
    }

    override fun inject(testInstance: Any?) {
        requireNotNull(testInstance)

        testInstance::class.declaredMemberProperties
            .filter { it.javaField?.isAnnotationPresent(InjectServer::class.java) ?: false }
            .map { it as KMutableProperty<*> }
            .forEach { it.setter.call(testInstance, server) }
    }

    override fun stop() {
        if (this::server.isInitialized) {
            server.stop()
        }
    }
}

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class InjectServer
