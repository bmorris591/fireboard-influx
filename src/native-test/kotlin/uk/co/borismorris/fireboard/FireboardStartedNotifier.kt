package uk.co.borismorris.fireboard

import io.quarkus.test.common.IntegrationTestStartedNotifier

class FireboardStartedNotifier : IntegrationTestStartedNotifier {
    override fun check(context: IntegrationTestStartedNotifier.Context?) = object : IntegrationTestStartedNotifier.Result {
        override fun isStarted() = true
        override fun isSsl() = false
    }
}
